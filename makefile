DPI=300

output=output/DPI_$(DPI)
spinner_frames_dir=$(output)/temp/spinner_frames
logo_frames_dir=$(output)/temp/logo_frames
frame_nums := 1 2 3 4 5 6 7 8
spinner_frames = $(patsubst %, $(spinner_frames_dir)/%.png, $(frame_nums))
logo_frames = $(patsubst %, $(logo_frames_dir)/light/%.png, $(frame_nums)) $(patsubst %, $(logo_frames_dir)/dark/%.png, $(frame_nums))

.PRECIOUS: $(spinner_frames) $(logo_frames) $(output)/temp/logo_text_light.png output/DPI_300/temp/logo_text_dark.png

$(spinner_frames_dir)/%.png: logo_animation.svg
	mkdir -p $(spinner_frames_dir)
	inkscape logo_animation.svg -i "frame $*" -j -C --export-png $(spinner_frames_dir)/$*.png --export-dpi=$(DPI)

$(output)/animated_spinner.webp: $(spinner_frames)
	ffmpeg -r 16.67 -i $(spinner_frames_dir)/\%d.png -lossless 1 -loop 0 -an -vsync 0 $@ -y

$(output)/temp/logo_text_%.png: logo.svg
	mkdir -p $(output)/temp
	inkscape logo.svg -i "text_$*" -j -C --export-png $@ --export-dpi=$(DPI)

$(logo_frames_dir)/light/%.png: $(output)/temp/logo_text_light.png $(spinner_frames)
	mkdir -p $(logo_frames_dir)/light
	convert -gravity southeast -composite $(output)/temp/logo_text_light.png $(spinner_frames_dir)/$*.png $@
$(logo_frames_dir)/dark/%.png: $(output)/temp/logo_text_dark.png $(spinner_frames)
	mkdir -p $(logo_frames_dir)/dark
	convert -gravity southeast -composite $(output)/temp/logo_text_dark.png $(spinner_frames_dir)/$*.png $@

$(output)/animated_logo_%.webp: $(logo_frames)
	mkdir -p $(output)
	ffmpeg -r 16.67 -i $(logo_frames_dir)/$*/\%d.png -lossless 1 -loop 0 -an -vsync 0 $@ -y

$(output)/logo_%.png: $(logo_frames)
	cp $(logo_frames_dir)/$*/1.png $@

.PHONY: all
all: $(output)/animated_logo_light.webp $(output)/animated_logo_dark.webp $(output)/animated_spinner.webp $(output)/logo_light.png $(output)/logo_dark.png

.PHONY: clean
clean:
	rm -rf output


.DEFAULT_GOAL := all
